class hashology::cloudwatch(
  $region = '', #ignored if location is onPremise
  $location = 'onPremise', #valid values: ec2 or onPremise
){
  
  tag 'monitoring', 'sysops'

  case $location == 'onPremise' {
    true: {
      $_location = 'onPremise'
      $_region = 'us-east-1'
      $_cw_config_file = 'puppet:///modules/hashology/cloudwatch/agent-config-onPremise.json'
    }
    default: {
      $_location = 'ec2'
      $_region = $region
      $_cw_config_file = 'puppet:///modules/hashology/cloudwatch/agent-config.json'
    }
  }



  file { '/opt/debs':
    ensure => directory
  }
  file { '/opt/debs/amazon-cloudwatch-agent.deb':
    ensure    => present,
    group     => root,
    mode      => '0644',
    owner     => root,
    source    => 'puppet:///modules/hashology/debs/amazon-cloudwatch-agent.deb',
    subscribe => File['/opt/debs']
  }
  package { 'amazon-cloudwatch-agent':
    ensure   => latest,
    provider => dpkg,
    source   => '/opt/debs/amazon-cloudwatch-agent.deb'
  }
  file { '/home/cwagent':
    ensure  => directory,
    group   => cwagent,
    owner   => cwagent,
    mode    => '0755',
    require => [Package['amazon-cloudwatch-agent']]
  }
  file { '/home/cwagent/.aws':
    ensure  => directory,
    group   => cwagent,
    owner   => cwagent,
    mode    => '0755',
    require => [Package['amazon-cloudwatch-agent']]
  }

  file { '/opt/aws/amazon-cloudwatch-agent/etc/common-config.toml':
    ensure  => present,
    group   => cwagent,
    mode    => '0644',
    owner   => cwagent,
    source  => 'puppet:///modules/hashology/cloudwatch/common-config.toml',
    require => [Package['amazon-cloudwatch-agent']],
    notify => Service["start-cloudwatch-agent"]
  }

  file { '/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json':
    ensure  => present,
    group   => cwagent,
    mode    => '0644',
    owner   => cwagent,
    source  => $_cw_config_file,
    require => [Package['amazon-cloudwatch-agent']],
    notify => Service["start-cloudwatch-agent"]
  }

  file { '/home/cwagent/.aws/credentials':
    ensure  => present,
    group   => cwagent,
    mode    => '0600',
    owner   => cwagent,
    source  => 'puppet:///modules/hashology/credentials/cloudwatch/credentials',
    require => [Package['amazon-cloudwatch-agent']],
    notify => Service["start-cloudwatch-agent"]
  }
  file_line { 'aws_region':
    ensure  => present,
    line    => "region=$_region",
    path    => '/home/cwagent/.aws/credentials',
    require => [File['/home/cwagent/.aws/credentials']],
    notify => Service["start-cloudwatch-agent"]
  }

  exec { 'cloudwatch-agent-configure':
    command   => "/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ${_location} -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json",
    require   => [
      File["/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json"],
      File["/opt/aws/amazon-cloudwatch-agent/etc/common-config.toml"],
      Package['amazon-cloudwatch-agent'],
      File_line['aws_region']
    ]
  }
  service { 'start-cloudwatch-agent':
    name => 'amazon-cloudwatch-agent',
    ensure => 'running',
    require => [Package['amazon-cloudwatch-agent'], Exec['cloudwatch-agent-configure']]
  }

}
